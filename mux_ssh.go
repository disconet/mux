package mux

import (
	"crypto"
	"crypto/sha256"
	"crypto/subtle"
	"errors"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io"
	"log"
	"net"
	"strings"
	"sync"
	"time"
)

const (
	versionString = "SSH-2.0-foo"
)

type sshMux struct {
	sshConn ssh.Conn

	clLk             sync.Mutex
	channelListeners map[string]*sshMuxChannelListener

	exitWg sync.WaitGroup
}

func (mux *sshMux) OpenChannel(name string) (MuxChannel, error) {
	ch, reqCh, err := mux.sshConn.OpenChannel(name, []byte{})
	if err != nil {
		return nil, transformError(err)
	}

	mux.exitWg.Add(1)
	go func() {
		defer mux.exitWg.Done()
		for {
			r, ok := <-reqCh
			if !ok {
				return
			}
			log.Printf("got unexpected out of band request %v on chan\n", r)
		}
	}()

	return ch, nil
}

func (mux *sshMux) ListenChannel(name string) (ChannelListener, error) {
	mux.clLk.Lock()
	defer mux.clLk.Unlock()
	if mux.channelListeners == nil {
		return nil, ErrMuxClosed
	}
	if mux.channelListeners[name] != nil {
		return nil, fmt.Errorf("a listener for '%s' already exists", name)
	}
	l := &sshMuxChannelListener{mux, name, make(chan ssh.NewChannel)}
	mux.channelListeners[name] = l
	return l, nil
}

func (mux *sshMux) Wait() error {
	mux.exitWg.Wait()
	return transformError(mux.sshConn.Wait())
}

func (mux *sshMux) Close() error {
	return mux.sshConn.Close()
}

func transformError(err error) error {
	if err == io.EOF {
		return ErrMuxClosed
	} else if strings.Contains(err.Error(), "use of closed network connection") {
		return ErrMuxClosed
	} else if e, ok := err.(*ssh.OpenChannelError); ok && e.Reason == ssh.UnknownChannelType {
		return ErrUnknownChannel
	} else if strings.HasPrefix(err.Error(), "ssh: unsupported key type") {
		return ErrUnsupportedKeyType
	}
	return err
}

type sshMuxChannelListener struct {
	mux  *sshMux
	name string
	ch   chan ssh.NewChannel
}

func (l *sshMuxChannelListener) Accept() (MuxChannel, error) {
	req, ok := <-l.ch
	if !ok {
		return nil, ErrMuxClosed
	}
	ch, reqs, err := req.Accept()
	if err != nil {
		return nil, err
	}
	go func() {
		for _ = range reqs {
			log.Println("discarding channel request")
		}
	}()
	return ch, nil
}

func (l *sshMuxChannelListener) Close() error {
	l.mux.clLk.Lock()
	defer l.mux.clLk.Unlock()

	delete(l.mux.channelListeners, l.name)

	select {
	case <-l.ch:
		return ErrMuxClosed
	default:
		close(l.ch)
	}
	return nil
}

func ClientMuxSSH(conn io.ReadWriteCloser, clientKey crypto.Signer, serverKey crypto.PublicKey) (Mux, error) {

	clientSSHSigner, err := sshSigner(clientKey)
	if err != nil {
		return nil, err
	}

	serverSSHPubKey, err := sshPubKey(serverKey)
	if err != nil {
		return nil, err
	}

	conf := &ssh.ClientConfig{
		ClientVersion: versionString,
		HostKeyCallback: func(hostname string, remote net.Addr, k ssh.PublicKey) error {
			return sshCheckKeysEqual(k, serverSSHPubKey)
		},
	}

	conf.Auth = []ssh.AuthMethod{ssh.PublicKeys(clientSSHSigner)}

	//conf.User = "user-abc"

	c, chans, reqs, err := ssh.NewClientConn(wrapConn(conn), "", conf)
	if err != nil {
		return nil, err
	}

	return newSSHMux(c, chans, reqs), nil
}

func ServerMuxSSH(conn io.ReadWriteCloser, serverKey crypto.Signer, clientKeys []crypto.PublicKey) (Mux, crypto.PublicKey, error) {
	serverSSHSigner, err := sshSigner(serverKey)
	if err != nil {
		return nil, nil, err
	}

	clientSSHPubKeys := make(map[string]ssh.PublicKey)
	cryptoPubkeys := make(map[string]crypto.PublicKey)

	for _, clientKey := range clientKeys {
		clientSSHPubKey, err := sshPubKey(clientKey)
		if err != nil {
			return nil, nil, err
		}
		ks := keyString(clientSSHPubKey)
		clientSSHPubKeys[ks] = clientSSHPubKey
		cryptoPubkeys[ks] = clientKey
	}

	var authenticatedPK crypto.PublicKey

	conf := &ssh.ServerConfig{
		NoClientAuth: false,
		PublicKeyCallback: func(md ssh.ConnMetadata, k ssh.PublicKey) (*ssh.Permissions, error) {
			ks := keyString(k)
			clientSSHPubKey := clientSSHPubKeys[ks]
			if clientSSHPubKey == nil {
				return nil, fmt.Errorf("authentication failed due to unknown key %s\n", ks)
			}

			err := sshCheckKeysEqual(k, clientSSHPubKey)
			if err == nil {
				authenticatedPK = cryptoPubkeys[ks]
			}
			return nil, err
		},
		AuthLogCallback: func(cmd ssh.ConnMetadata, method string, err error) {
			var message string
			if err != nil {
				message = fmt.Sprintf("failed with error %s", err.Error())
			} else {
				message = "succeeded"
			}
			log.Printf("login by user %v from %v with version %s using %v %s", cmd.User(), cmd.RemoteAddr(), cmd.ClientVersion(), method, message)
		},
		ServerVersion: versionString,
	}
	conf.AddHostKey(serverSSHSigner)

	serverConn, newChan, Req, err := ssh.NewServerConn(wrapConn(conn), conf)
	if err != nil {
		return nil, nil, err
	}

	return newSSHMux(serverConn, newChan, Req), authenticatedPK, nil
}

func keyString(key ssh.PublicKey) string {
	keyPart := fmt.Sprintf("%x", sha256.Sum256(key.Marshal()))
	return fmt.Sprintf("%s %s", key.Type(), keyPart)
}

func newSSHMux(conn ssh.Conn, newChan <-chan ssh.NewChannel, reqs <-chan *ssh.Request) Mux {
	mux := &sshMux{
		sshConn:          conn,
		channelListeners: make(map[string]*sshMuxChannelListener),
	}
	mux.exitWg.Add(1)
	go func() {
		defer mux.exitWg.Done()
		for r := range reqs {
			log.Printf("discarding global request:%v", r.Type)
			r.Reply(false, nil)
		}
	}()
	mux.exitWg.Add(1)
	go func() {
		defer mux.exitWg.Done()
		for ch := range newChan {
			mux.clLk.Lock()
			listener := mux.channelListeners[ch.ChannelType()]
			mux.clLk.Unlock()

			if listener != nil {
				listener.ch <- ch
			} else {
				ch.Reject(ssh.UnknownChannelType, fmt.Sprintf("unknown channel type: %v", ch.ChannelType()))
			}
		}
		mux.clLk.Lock()
		for _, cl := range mux.channelListeners {
			close(cl.ch)
		}
		mux.channelListeners = nil
		mux.clLk.Unlock()
	}()
	return mux
}

func sshCheckKeysEqual(k1, k2 ssh.PublicKey) error {
	if k1.Type() != k2.Type() {
		return fmt.Errorf("mismatched key key types %v and %v", k1.Type(), k2.Type())
	}

	// TODO: does this need to be constant time? Is this really a risk here?
	if subtle.ConstantTimeCompare(k1.Marshal(), k2.Marshal()) != 1 {
		return fmt.Errorf("key mismatch :\n%x\n%x\n", k1.Marshal(), k2.Marshal())
	}
	return nil
}

func sshSigner(s crypto.Signer) (ssh.Signer, error) {
	signer, err := ssh.NewSignerFromKey(s)
	if err != nil {
		return nil, transformError(err)
	}
	return signer, nil
}

func sshPubKey(pub crypto.PublicKey) (ssh.PublicKey, error) {
	pubkey, err := ssh.NewPublicKey(pub)
	if err != nil {
		return nil, transformError(err)
	}
	return pubkey, nil
}

func wrapConn(rwc io.ReadWriteCloser) net.Conn {
	switch x := rwc.(type) {
	case net.Conn:
		return x
	default:
		return wrappedConn{rwc}
	}
}

type wrappedConn struct {
	io.ReadWriteCloser
}

func (fc wrappedConn) RemoteAddr() net.Addr {
	return &net.TCPAddr{IP: net.IPv4zero, Port: 0}
}

func (fc wrappedConn) LocalAddr() net.Addr {
	return &net.TCPAddr{IP: net.IPv4zero, Port: 0}
}

func (fc wrappedConn) SetDeadline(t time.Time) error {
	return errors.New("deadline not implemented")
}

func (fc wrappedConn) SetReadDeadline(t time.Time) error {
	return errors.New("deadline not implemented")
}

func (fc wrappedConn) SetWriteDeadline(t time.Time) error {
	return errors.New("deadline not implemented")
}
