package mux

import (
	"errors"
	"io"
)

var (
	ErrMuxClosed          = errors.New("connection closed")
	ErrUnknownChannel     = errors.New("unknown channel")
	ErrUnsupportedKeyType = errors.New("unsupported key type")
)

// Mux multiplexes a reliable ordered full-duplex connection into multiple
// streams.
type Mux interface {
	// Open a new channel of a given type
	OpenChannel(name string) (MuxChannel, error)
	// Listen for new channel of a given type
	ListenChannel(name string) (ChannelListener, error)
	// Wait until the mux is closed and return any error that cause it
	// Typically this happens when the underlying transport has closed
	Wait() error
	// Close closes the mux
	Close() error
}

type MuxChannel interface {
	io.ReadWriteCloser
	CloseWrite() error
}

type ChannelListener interface {
	Close() error
	Accept() (MuxChannel, error)
}
