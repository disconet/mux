package mux

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"errors"
	"github.com/stretchr/testify/assert"
	"io"
	"io/ioutil"
	"net"
	"testing"
	"time"
)

func TestRefuseListenWhenConnClosed(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	mustClose(t, ccon)
	mustClose(t, scon)

	// either the mux should refuse listener immediately, or it should be
	// found closed on attempted use

	if cl, err := cmux.ListenChannel("test"); err == nil {
		if _, err = cl.Accept(); err == nil {
			t.Error("expected an error but got nil")
		}
	}

	if sl, err := smux.ListenChannel("test"); err == nil {
		if _, err = sl.Accept(); err == nil {
			t.Error("expected an error but got nil")
		}
	}

	assert.Equal(ErrMuxClosed, cmux.Wait())
	assert.Equal(ErrMuxClosed, smux.Wait())
}

func TestAcceptExitsOnCloseListener(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	_, smux := newClientAndServer(ccon, scon)
	defer mustClose(t, ccon)
	defer mustClose(t, scon)

	l, err := smux.ListenChannel("dummy")
	assert.NoError(err)

	acceptErr := make(chan error)
	go func() {
		_, err := l.Accept()
		acceptErr <- err
	}()

	assert.NoError(l.Close())

	assert.Error(<-acceptErr)
}

func TestCloseListenerTwiceIsError(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	_, smux := newClientAndServer(ccon, scon)
	defer mustClose(t, ccon)
	defer mustClose(t, scon)

	l, err := smux.ListenChannel("dummy")
	assert.NoError(err)

	assert.NoError(l.Close())
	assert.Error(l.Close())
}

func TestAddListenerTwiceIsError(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	if _, err := smux.ListenChannel("dummy"); err != nil {
		t.Error(err)
	}
	if _, err := smux.ListenChannel("dummy"); err == nil {
		t.Error("expected error but got nil")
	}

	mustClose(t, ccon)
	mustClose(t, scon)

	assert.Equal(ErrMuxClosed, cmux.Wait())
	assert.Equal(ErrMuxClosed, smux.Wait())

}

func TestListenAfterCloseIsError(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	mustClose(t, ccon)
	mustClose(t, scon)

	smux.Wait()
	cmux.Wait()

	_, err := cmux.ListenChannel("dummy")
	assert.Equal(err, ErrMuxClosed)
	_, err = smux.ListenChannel("dummy")
	assert.Equal(err, ErrMuxClosed)
}

func TestDialAfterCloseIsError(t *testing.T) {
	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	mustClose(t, ccon)
	mustClose(t, scon)

	smux.Wait()
	cmux.Wait()

	_, err := cmux.OpenChannel("unknown")
	if err != ErrMuxClosed {
		t.Error(err)
	}
}

func TestDialUnknownChannel(t *testing.T) {
	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	defer func() {
		mustClose(t, ccon)
		mustClose(t, scon)
		smux.Wait()
		cmux.Wait()
	}()

	_, err := cmux.OpenChannel("unknown")
	if err != ErrUnknownChannel {
		t.Error(err)
	}
}

func TestSSHMuxEcho(t *testing.T) {
	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)
	defer mustClose(t, ccon)
	defer mustClose(t, scon)

	doneServer := make(chan struct{})
	serverErr := make(chan error)

	l, err := smux.ListenChannel("echo")
	if err != nil {
		serverErr <- err
		return
	}

	go func() {
		if err := listenOneAndClose(l); err != nil {
			serverErr <- err
		}
		close(doneServer)
	}()

	msg := "hi world\nhow are you?\n"

	echoed, err := echoRequest(cmux, msg)
	if err != nil {
		t.Error(err)
	}

	if echoed != msg {
		t.Error("echo failed")
	}

	select {
	case <-doneServer:
	case err := <-serverErr:
		t.Error(err)
	}
}

func TestCloseClosesChannel(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	l, err := smux.ListenChannel("test")
	if err != nil {
		t.Fatal(err)
	}

	accErr := make(chan error)
	readErr := make(chan error)
	accepted := make(chan struct{})

	go func() {
		con, err := l.Accept()
		if err != nil {
			accErr <- err
			return
		}
		accepted <- struct{}{}
		buf := make([]byte, 1024)
		_, re := con.Read(buf)
		readErr <- re
	}()

	go func() {
		_, err := cmux.OpenChannel("test")
		if err != nil {
			t.Log("dial channel failed. expect accept to fail now.")
			return
		}
		cmux.Wait()
	}()

	select {
	case <-time.After(5 * time.Second):
		t.Fatal("accept didn't happen in time")
	case err := <-accErr:
		t.Errorf("unexpected accept error : %v", err)
	case <-accepted:
	}

	assert.NoError(smux.Close())

	select {
	case <-time.After(5 * time.Second):
		t.Error("read did not error after close")
	case err := <-readErr:
		assert.Equal(err, io.EOF)
	}

	assert.EqualError(smux.Wait(), "connection closed")
	assert.EqualError(cmux.Wait(), "connection closed")

	assert.Error(ccon.Close())
	assert.Error(scon.Close())
}

func TestCloseClosesListeners(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)

	l, err := smux.ListenChannel("test")
	if err != nil {
		t.Fatal(err)
	}

	accErr := make(chan error)
	go func() {
		_, err := l.Accept()
		accErr <- err
	}()

	smux.Close()

	select {
	case <-time.After(5 * time.Second):
		t.Error("accept did not return after close")
	case err := <-accErr:
		assert.EqualError(err, "connection closed")
	}

	assert.EqualError(smux.Wait(), "connection closed")
	assert.EqualError(cmux.Wait(), "connection closed")

	assert.Error(ccon.Close())
	assert.Error(scon.Close())
}

func TestManyChannelOpen(t *testing.T) {
	assert := assert.New(t)

	const MAX = 10000

	ccon, scon := newTCPConns()
	cmux, smux := newClientAndServer(ccon, scon)
	defer func() {
		mustClose(t, ccon)
		mustClose(t, scon)
		cmux.Wait()
		smux.Wait()
	}()

	doneServer := make(chan struct{})
	serverErr := make(chan error)

	l, err := smux.ListenChannel("echo")
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		for i := 0; i < MAX; i++ {
			chanConn, err := l.Accept()
			if err != nil {
				serverErr <- err
				return
			}
			go func() {
				if _, err = io.Copy(chanConn, chanConn); err != nil {
					serverErr <- err
					return
				}
				if err := chanConn.Close(); err != nil {
					serverErr <- err
					return
				}
			}()
		}
		if err := l.Close(); err != nil {
			serverErr <- err
			return
		}
		close(doneServer)
	}()

	var mcs []MuxChannel
	msg := "hi world\nhow are you?\n"

	var cch MuxChannel
	for i := 0; i < MAX; i++ {
		var err error
		cch, err = cmux.OpenChannel("echo")
		if err != nil {
			t.Error(err)
		}
		mcs = append(mcs, cch)
	}

	for _, ch := range mcs {
		if _, err := ch.Write([]byte(msg)); err != nil {
			t.Error(err)
		}
		if err := ch.CloseWrite(); err != nil {
			t.Error(err)
		}
		echoed, err := ioutil.ReadAll(ch)
		if err != nil {
			t.Error(err)
		}

		assert.Equal(msg, string(echoed))
	}

	select {
	case <-doneServer:
	case err := <-serverErr:
		t.Error(err)
	}
}

func TestGetsCorrectAuthenticatedPublicKey(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()

	clientKey1 := newKey()
	clientKey2 := newKey()
	clientKey3 := newKey()

	serverKey := newKey()

	cmuxCh := make(chan Mux, 1)

	go func() {
		cmux, err := ClientMuxSSH(ccon, clientKey2, serverKey.Public())
		if err != nil {
			panic(err)
		}
		cmuxCh <- cmux
	}()

	smux, authKey, err := ServerMuxSSH(scon, serverKey, []crypto.PublicKey{clientKey1.Public(), clientKey2.Public(), clientKey3.Public()})
	if err != nil {
		t.Fatal(err)
	}

	cmux := <-cmuxCh

	assert.Equal(authKey, clientKey2.Public())

	mustClose(t, ccon)
	mustClose(t, scon)
	cmux.Wait()
	smux.Wait()

}

func listenOneAndClose(l ChannelListener) error {
	chanConn, err := l.Accept()
	if err != nil {
		return err
	}
	if _, err = io.Copy(chanConn, chanConn); err != nil {
		return err
	}
	if err := chanConn.Close(); err != nil {
		return err
	}
	if err := l.Close(); err != nil {
		return err
	}

	return nil
}

func TestUnsupportedPublic(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	defer mustClose(t, ccon)
	defer mustClose(t, scon)

	goodKey := newKey()

	_, err := ClientMuxSSH(ccon, goodKey, badPublic{})
	assert.Equal(ErrUnsupportedKeyType, err)

	_, authKey, err := ServerMuxSSH(scon, goodKey, []crypto.PublicKey{badPublic{}})
	assert.Equal(ErrUnsupportedKeyType, err)
	assert.Nil(authKey)
}

func TestUnsupportedSigner(t *testing.T) {
	assert := assert.New(t)

	ccon, scon := newTCPConns()
	defer mustClose(t, ccon)
	defer mustClose(t, scon)

	goodKey := newKey()

	_, err := ClientMuxSSH(ccon, badSigner{}, goodKey.Public())
	assert.Equal(ErrUnsupportedKeyType, err)

	_, authKey, err := ServerMuxSSH(scon, badSigner{}, []crypto.PublicKey{goodKey.Public()})
	assert.Equal(ErrUnsupportedKeyType, err)
	assert.Nil(authKey)
}

type badSigner struct{}

func (bs badSigner) Public() crypto.PublicKey {
	return badPublic{}
}
func (bs badSigner) Sign(io.Reader, []byte, crypto.SignerOpts) ([]byte, error) {
	return nil, errors.New("should not reach here")
}

type badPublic struct{}

func echoRequest(mux Mux, msg string) (string, error) {

	ch, err := mux.OpenChannel("echo")
	if err != nil {
		return "", err
	}

	if _, err := ch.Write([]byte(msg)); err != nil {
		return "", err
	}
	if err := ch.CloseWrite(); err != nil {
		return "", err
	}
	echoed, err := ioutil.ReadAll(ch)
	if err != nil {
		return "", err
	}
	return string(echoed), nil
}

func newKey() crypto.Signer {
	pk, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		panic(err)
	}
	return pk
}

// returns a connected local tcp connection pair
func newTCPConns() (io.ReadWriteCloser, io.ReadWriteCloser) {
	l, err := net.Listen("tcp", ":2222")
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := l.Close(); err != nil {
			panic(err)
		}
	}()

	sconnCh := make(chan io.ReadWriteCloser)
	go func() {
		tcpConn, err := l.Accept()
		if err != nil {
			panic(err)
		}
		sconnCh <- tcpConn
	}()

	cconn, err := net.Dial("tcp", "localhost:2222")
	if err != nil {
		panic(err)
	}

	return cconn, <-sconnCh

}

func newClientAndServer(c1, c2 io.ReadWriteCloser) (Mux, Mux) {
	clientKey := newKey()
	serverKey := newKey()

	cmuxCh := make(chan Mux)
	go func() {

		cmux, err := ClientMuxSSH(c1, clientKey, serverKey.Public())
		if err != nil {
			panic(err)
		}

		cmuxCh <- cmux
	}()

	smux, _, err := ServerMuxSSH(c2, serverKey, []crypto.PublicKey{clientKey.Public()})
	if err != nil {
		panic(err)
	}
	return <-cmuxCh, smux
}

func mustClose(t *testing.T, closer io.Closer) {
	if err := closer.Close(); err != nil {
		t.Error(err)
	}
}
